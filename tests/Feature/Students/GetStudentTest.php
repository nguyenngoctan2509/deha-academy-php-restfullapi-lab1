<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetStudentTest extends TestCase
{
    /** @test  */

    public function user_can_get_student_if_it_exist(){
        $student = Student::factory()->create();

        $response = $this->getJson(route('students.show',$student->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('data',fn(AssertableJson $json) =>
        $json->where('name',$student->name)
            ->etc()
        )
        );
    }

    /** @test */

    public function user_cant_get_student_if_it_not_exist(){
        $studentId = -1;
        $response = $this->getJson(route('students.show',$studentId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
