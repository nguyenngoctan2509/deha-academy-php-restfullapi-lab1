<?php

namespace Tests\Feature\Students;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateStudentTest extends TestCase
{
    /** @test  */

    public function user_can_create_student_if_data_is_valid(){
        $dataCreate =[
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2)
        ];

        $response = $this->json('POST',route('students.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('data', fn(AssertableJson $json)=>
        $json->where('name',$dataCreate['name'])
            ->etc()
        )->etc()
        );

        $this->assertDatabaseHas('students',[
            'name' => $dataCreate['name'],
            'age' => $dataCreate['age']
        ]);
    }

    /** @test  */

    public function user_cant_create_student_if_name_is_null(){
        $dataCreate =[
            'name' => '',
            'age' => $this->faker->randomNumber(2)
        ];

        $response = $this->json('POST',route('students.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors',fn(AssertableJson $json)=>
        $json->has('name')
            ->etc()
        )->etc()
        );
    }

    /** @test  */

    public function user_cant_create_student_if_age_is_null(){
        $dataCreate =[
            'name' => $this->faker->name,
            'age' => ''
        ];

        $response = $this->json('POST',route('students.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors',fn(AssertableJson $json)=>
        $json->has('age')
            ->etc()
        )->etc()
        );
    }

    /** @test  */

    public function user_cant_create_student_if_data_is_null(){
        $dataCreate = [
            'name' => '',
            'age' => ''
        ];

        $response = $this->json('POST',route('students.store'),$dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors',fn(AssertableJson $json)=>
        $json->has('age')
            ->has('name')
            ->etc()
        )->etc()
        );
    }

}
