<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateStudentTest extends TestCase
{
    /** @test  */

    public function user_can_update_student_if_student_exist_and_data_is_valid(){
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2)
        ];

        $response = $this->json('PUT',route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('data',fn(AssertableJson $json)=>
        $json->where('name',$dataUpdate['name'])
            ->etc()
        )->etc()
        );

        $this->assertDatabaseHas('students',[
            'name' => $dataUpdate['name'],
            'age' => $dataUpdate['age']
        ]);
    }

    /** @test  */

    public function user_cant_update_student_if_name_is_null(){
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => '',
            'age' => $this->faker->randomNumber(2)
        ];

        $response = $this->json('PUT',route('students.update',$student->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) => $json ->has('errors',fn(AssertableJson $json) => $json ->has('name')));
    }

    /** @test  */

    public function user_cant_update_student_if_age_is_null(){
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => ''
        ];

        $response = $this->json('PUT',route('students.update',$student->id),$dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(fn(AssertableJson $json) => $json ->has('errors',fn(AssertableJson $json) => $json ->has('age')));
    }
    /** @test  */

    public function user_cant_update_student_if_student_exist_and_data_is_null(){
        $student = Student::factory()->create();

        $dataUpdate = [
            'name' => '',
            'age' => ''
        ];

        $response = $this->json('PUT',route('students.update',$student->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('errors',fn(AssertableJson $json)=>
        $json->has('name') ->has('age')
        )
        );
    }

    /** @test  */
    public function user_cant_update_student_if_student_not_exist(){
        $studentId = -1;
        $dataUpdate = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2)
        ];

        $response = $this->json('PUT',route('students.update',$studentId),$dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
