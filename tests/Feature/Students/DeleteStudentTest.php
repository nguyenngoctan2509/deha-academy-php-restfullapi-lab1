<?php

namespace Tests\Feature\Students;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteStudentTest extends TestCase
{
    /** @test  */

    public function user_can_delete_student_if_exists(){
        $student = Student::factory()->create();
        $studentBeforeDel = Student::count();

        $response = $this->json('DELETE',route('students.destroy',$student->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn(AssertableJson $json)=>
        $json->has('data',fn(AssertableJson $json) =>
        $json->where('name',$student->name) ->etc()
        )->etc()
        );
        $studentAfterDel = Student::count();
        $this->assertEquals($studentBeforeDel-1,$studentAfterDel);
    }

    /** @test  */

    public function user_cant_delete_student_if_not_exists(){
        $studentId = -1;

        $response = $this->json('DELETE',route('students.destroy',$studentId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
