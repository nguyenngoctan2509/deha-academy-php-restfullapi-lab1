<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    protected $student;

    /**
     * @param $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function index()
    {
        $students = $this->student->paginate(5);
        $studentsResource = StudentResource::collection($students)->response()->getData(true);
        //k response->getData() se chi co du lieu , 0 link ,0 data
//        return response()->json([
//            'data' => $studentsResource
//        ], Response::HTTP_OK);

        return $this->sentSuccessResponse($studentsResource,'Success',Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function store(StoreStudentRequest $request)
    {
        $dataCreate = $request->all();
        $student = $this->student->create($dataCreate);

        $studentResource = new StudentResource($student);
        return response()->json([
            'data' => $studentResource
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function show($id)
    {
        $student = $this->student->findOrFail($id);

        $studentResource = new StudentResource($student);
        return response()->json([
            'data' => $studentResource
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update(UpdateStudentRequest $request, $id)
    {
        $student = $this->student->findOrFail($id);

        $dataUpdate = $request->all();

        $student->update($dataUpdate);

        $studentResource = new StudentResource($student);
        return response()->json([
            'data' => $studentResource
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function destroy($id)
    {
        $student = $this->student->findOrFail($id);
        $student->delete();

        $studentResource = new StudentResource($student);
        return response()->json([
            'data' => $studentResource,
            'message'=>'Delete success'
        ], Response::HTTP_OK);
    }
}
